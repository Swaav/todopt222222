import React, { Component } from "react";
import TodoItem from "./TodoItem.jsx"

class TodoList extends Component {
    render() {
        return (
            <section className="main">
                <ul className="todo-list">
                    {this.props.todos.map(todo => (
                        <TodoItem title={todo.title}
                            completed={todo.completed}
                            handleToggleClick={this.props.handleToggleClick(todo)}
                            handleDestroyClick={this.props.handleDestroyClick(todo)} />
                    ))}
                </ul>
            </section>
        );
    }
}

export default TodoList